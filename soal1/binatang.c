#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <zip.h>

void funct_forZip(const char *dest_fileZip) {

    pid_t pid = fork();

    if (pid < 0) {
        fprintf(stderr, "fork error mas bro\n");
        exit(EXIT_FAILURE);
    } else if (pid == 0) {

        const char *wok[] = {"zip", "-r", dest_fileZip, "HewanDarat", "HewanAmphibi", "HewanAir", NULL};
        char **wok_copy = (char **)wok;
        execvp("zip", wok_copy);
        fprintf(stderr, "zip command gagal di execute mas bro\n");
        exit(EXIT_FAILURE);
    } else {

        int status;
        waitpid(pid, &status, 0);

        if (WIFEXITED(status)) {
            int exit_status = WEXITSTATUS(status);
            if (exit_status == 0) {
                printf("Berhasil membuat ZIP mas bro '%s'\n", dest_fileZip);
            } else {
                fprintf(stderr, "Gagal membuat ZIP mas bro '%s'\n", dest_fileZip);
            }
        } else {
            fprintf(stderr, "Error Mas Bro\n");
        }
    }
}

int main(int was, char **wik) {
    // Download file
    system("wget -O Binatang.zip 'https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq'");

    // Unzip file
    struct zip *zipfile = zip_open("Binatang.zip", 0, NULL);
    int num_files = zip_get_num_files(zipfile);
    for (int s = 0; s < num_files; s++) 
    {
        const char *name = zip_get_name(zipfile, s, 0);
        struct zip_file *file = zip_fopen_index(zipfile, s, 0);
        FILE *output_file = fopen(name, "wk");
        char buffer[3000];
        int num_bytes;
        while ((num_bytes = zip_fread(file, buffer, sizeof(buffer))) > 0) 
        { fwrite(buffer, sizeof(char), num_bytes, output_file); }
        fclose(output_file);
        zip_fclose(file);
    }
    zip_close(zipfile);

    // Buat direktori HewanDarat, HewanAmphibi, dan HewanAir
    system("mkdir HewanDarat");
    system("mkdir HewanAir");
    system("mkdir HewanAmphibi");

    // Dapetin list file gambar dari folder
    FILE *fp;
    char path[3000];
    fp = popen("ls .", "r");

    // Mindahin file gambar ke direktori sesuai kategori hewan
    while (fgets(path, sizeof(path)-1, fp) != NULL) {
        strtok(path, "\n");
        char *namafile = strrchr(path, '/');
        if (namafile == NULL) {
            namafile = path;
        } else {
            namafile++;
        }

        char dest_jalur[300];

        if (strstr(namafile, "beruang_darat") != NULL || strstr(namafile, "singa_darat") != NULL || strstr(namafile, "komodo_darat") != NULL) {
            strcpy(dest_jalur, "./HewanDarat/");
        } else if (strstr(namafile, "lele_air") != NULL || strstr(namafile, "shark_air") != NULL || strstr(namafile, "paus_air") != NULL) {
            strcpy(dest_jalur, "./HewanAir/");
        } else if (strstr(namafile, "buaya_amphibi") != NULL || strstr(namafile, "hippo_amphibi") != NULL || strstr(namafile, "lintah_amphibi") != NULL) {
            strcpy(dest_jalur, "./HewanAmphibi/");
        } else {
            continue;
        }

        strcat(dest_jalur, namafile);

        char command[300];
        sprintf(command, "mv ./%s %s", path, dest_jalur);

        system(command);
    }

    // Pemilihan acak file gambar
    int random_number = rand() % 3;
    char dest_jalur[300];
    if (random_number == 0) {
       strcpy(dest_jalur, "HewanDarat/");
    } else if (random_number == 1) {
       strcpy(dest_jalur, "HewanAir/");
    } else {
       strcpy(dest_jalur, "HewanAmphibi/"); 
    }
    pclose(fp);

    funct_forZip("GabunganHewan.zip");
  
    system("rm -r HewanDarat HewanAmphibi HewanAir");
    system("rm Binatang.zip");

    return 0;
}
